.PHONY: help

HOSTNAME=eu.gcr.io
PROJECT_ID=sample-iac

help: ## Show Help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

info: ## print gcp info
	gcloud config list

build: ## build docker image
	docker build -t ${HOSTNAME}/${PROJECT_ID}/sandbox-app:v1 .

push: ## push the docker image to artifactory
	docker push ${HOSTNAME}/${PROJECT_ID}/sandbox-app:v1

rm: ## delete the image from host
	docker rmi -f  ${HOSTNAME}/${PROJECT_ID}/sandbox-app:v1

bash: ## run bash into the image
	docker run -it  ${HOSTNAME}/${PROJECT_ID}/sandbox-app:v1 bash

public_ip: ## display cluster IP
	gcloud --format="value(networkInterfaces[0].accessConfigs[0].natIP)" compute instances list --filter="tags.items=iac-kubernetes"

# gcloud auth configure-docker
# gcloud auth login
