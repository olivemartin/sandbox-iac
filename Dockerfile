# base node image
FROM node:12

# install required system packages
RUN apt-get update -qq && \
    apt-get install -y build-essential

# create application directory and install dependencies
ENV APP_HOME /app
RUN mkdir $APP_HOME
WORKDIR $APP_HOME
COPY app/package.json $APP_HOME/
RUN npm install

# copy the application code to the container
ADD app/ $APP_HOME

# Run "npm start" command on container's start
CMD [ "npm", "start" ]
