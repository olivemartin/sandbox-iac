import express from 'express'
import bodyParser from 'body-parser'
import mongoose, { Schema } from 'mongoose'

const StationSchema = new Schema({ name: { type: String } })
const Station = mongoose.model('Station', StationSchema)
const app = express()

app.use(bodyParser.json())

app.get('/', (req, res) => {
  Station.find((err, stations) => {
    res.json(stations)
  })
})

const port = 9292
const dbHost = process.env.DATABASE_HOST || 'localhost'

console.log('Connecting to mongodb')
mongoose.connect(`mongodb://${dbHost}/iac`, {
  useNewUrlParser: true,
  useUnifiedTopology: true
}).then(() => {
  // Mongoose
  const hello = new Station({name: 'Hello world!'})
  hello.save()

  // Express
  app.listen(port, err => {
    if (err) {
      return console.error(err)
    }
    return console.log(`Server is listening on ${port}`)
  })
})
